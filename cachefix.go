// ありがとうジャバにアップロードしたファイルが
// gsutilの謎仕様により
// 圧縮して配置した場合 cache-controlに no-transform がついていた
// すでにアップロード済みのものについて、cache-controlを修正する
// 新しいものはRustで書いた最高のツールでS3互換エンドポイントでアップロードしてるのでこの問題は発生しない
package main

import (
	"context"
	"log"
	"strings"

	"cloud.google.com/go/storage"
	"google.golang.org/api/iterator"
)

func main() {
	ctx := context.Background()
	client, err := storage.NewClient(ctx)
	if err != nil {
		log.Fatal(err)
	}
	defer client.Close()

	bkt := client.Bucket("arigato-java.download")
	query := &storage.Query{Prefix: ""}

	it := bkt.Objects(ctx, query)
	for {
		attrs, err := it.Next()
		if err == iterator.Done {
			break
		}
		if err != nil {
			log.Fatal(err)
		}
		if len(attrs.CacheControl) == 0 {
			continue
		}
		CacheControlElements := strings.Split(attrs.CacheControl, ",")
		CacheControlOut := make([]string, 0)
		for _, e := range CacheControlElements {
			if e == "no-transform" {
				continue
			}
			CacheControlOut = append(CacheControlOut, e)
		}
		if len(CacheControlElements) == len(CacheControlOut) {
			continue
		}
		CacheControlNew := strings.Join(CacheControlOut, ",")
		log.Printf("%s: %s => %s\n", attrs.Name, attrs.CacheControl, CacheControlNew)

		obj := bkt.Object(attrs.Name)

		AttrsToUpdate := storage.ObjectAttrsToUpdate{CacheControl: CacheControlNew}
		_, err = obj.Update(ctx, AttrsToUpdate)
		if err != nil {
			log.Fatal(err)
		}
	}
}
